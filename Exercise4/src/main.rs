// use std::sync::mpsc;
// use std::thread;
// use std::time::Duration;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let mut children = Vec::new();
    let (tx, rx) = std::sync::mpsc::channel();

    for i in 0..N {
        let tx1 = std::sync::mpsc::Sender::clone(&tx);
        children.push(std::thread::spawn(move || {
            let message = format!("This is thread: {}", i);
            tx1.send(message).unwrap();
            std::thread::sleep(std::time::Duration::from_millis(1000));
        }));
    }

    for child in children {
        child.join().unwrap();
    }

    drop(tx);
    
    for received in rx {
        println!("Got: {}", received);
    }
}
