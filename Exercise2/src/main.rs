// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut cache = vec![0, 1];
    let index = n as usize;
    for i in 2..(index + 1) {
        cache.push(cache[i - 1] + cache[i - 2]);
    }
    return cache[index];
}

fn main() {
    println!("{}", fibonacci_number(10));
}
